//Trainees to create
const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderController = require("../controllers/orderController");

router.post("/checkout", auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	orderController.checkout(userId, req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/", auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;

	orderController.getOrders(userId).then(resultFromController => res.send(resultFromController));
})

module.exports = router;