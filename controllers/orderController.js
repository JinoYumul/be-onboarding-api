//Trainees to create
const Order = require("../models/orders");

module.exports.checkout = (userId, body) => {
    let newOrder = new Order({
        userId: userId,
        products: body.products,
        totalAmount: body.totalAmount
    })

    return newOrder.save().then((order, err) => {
        if(err){
            return false;
        }else{
            return true;
        }
    })
}

module.exports.getOrders = (userId) => {
	return Order.find({userId: userId}).then(orders => {
		if(orders.length <= 0){
			return false;
		} else {
			return orders;
		}
	})
}