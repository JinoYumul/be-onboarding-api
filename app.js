//Server file will be provided to trainees, but they will still need to add their own order routes
const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoutes");
const orderRoutes = require("./routes/orderRoutes");

mongoose.connect("mongodb+srv://jino:jino123@cluster1.7j4kj.mongodb.net/ecommerce_test?retryWrites=true&w=majority", {
	useNewUrlParser: true,	
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}))

app.use("/users", userRoutes);
app.use("/orders", orderRoutes);

app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});